import React, { Component } from "react";
import "./styles.css";

class List extends Component {
  state = {
    countries: [],
    global: {}
  };

  componentDidMount() {
    fetch("https://coronavirus-19-api.herokuapp.com/countries")
      .then(res => res.json())
      .then(res => {
        this.setState({
          countries: res
        });
      });
    fetch("https://coronavirus-19-api.herokuapp.com/all")
      .then(res => res.json())
      .then(res => {
        this.setState({
          global: res
        });
      });
  }

  render() {
    let content = "";
    let contentGlobal = "";
    contentGlobal = (
      <ul>
        <li className="global-list" key={this.state.global.id}>
          <p className="global-name">
            <b>Número de casos:</b> {this.state.global.cases}
          </p>
          <p className="global-name">
            <b>Mortes:</b> {this.state.global.deaths}
          </p>
          <p className="global-name">
            <b>Recuperados:</b> {this.state.global.recovered}
          </p>
        </li>
      </ul>
    );
    if (this.state.countries.length !== 0) {
      content = (
        <ul>
          {this.state.countries.map(item => (
            <div id="country">
              <li key={item.id}>
                <p className="country-name">{item.country}</p>
                <p className="country-cases">
                  <b>Casos confirmados:</b> {item.cases}
                </p>
                <p className="country-deaths">
                  <b>Mortes:</b> {item.deaths}
                </p>
                <p className="country-todayDeaths">
                  <b>Mortes no dia:</b> {item.todayDeaths}
                </p>
                <p className="country-active">
                  <b>Não curados:</b> {item.active}
                </p>
                <p className="country-critical">
                  <b>Estado crítico:</b> {item.critical}
                </p>
              </li>
            </div>
          ))}
        </ul>
      );
    }

    return (
      <div className="content">
        <h2 className="global-title">Casos no mundo</h2>
        <section className="global">{contentGlobal}</section>
        <section className="countries">
          <h2 className="countries-title">Lista de países</h2>
          {content}
        </section>
      </div>
    );
  }
}

export default List;
