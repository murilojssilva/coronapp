import React from "react";
import "./styles.css";

export default function Footer() {
  return (
    <div className="footer">
      <p className="text">
        Desenvolvido por: <a href="https://gitlab.com/murilojssilva">Murilo</a>
      </p>
    </div>
  );
}
