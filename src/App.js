import React from "react";
import "./App.css";
import img from "./assets/coronavirus.png";
import List from "./components/List/index";
import Footer from "./components/Footer";

export default function App() {
  return (
    <>
      <h1 className="title">Coronapp</h1>
      <img src={img} className="img" alt="Coronavirus" />
      <List />
      <Footer />
    </>
  );
}
